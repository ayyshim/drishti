import 'dart:io';

import 'package:alan_voice/alan_voice.dart';
import 'package:face_net_authentication/pages/widgets/app_button.dart';
import 'package:face_net_authentication/utils.dart';
import 'package:flutter/material.dart';

import 'home.dart';

class Profile extends StatefulWidget {
  const Profile(this.username, {Key key, this.imagePath}) : super(key: key);
  final String username;
  final String imagePath;

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  String response;

  @override
  void initState() {
    super.initState();
    _setupAlan();
  }

  void _setupAlan() {
    AlanVoice.addButton(
      "eb4e461cf9887691f117cb2b770266502e956eca572e1d8b807a3e2338fdd0dc/stage",
      buttonAlign: AlanVoice.BUTTON_ALIGN_LEFT,
    );

    AlanVoice.callbacks.add((command) => _handleCommand(command.data));
  }

  void _handleCommand(Map<String, dynamic> data) {
    print("Callback called");
    if (data["command"] == "call") {
      makeCall(data["value"]).then((value) {
        this.response = value;
        setState(() {});
        AlanVoice.playText(this.response);
      });
    } else if (data["command"] == "search") {
      webSearch(data["value"]).then((value) {
        this.response = value;
        setState(() {});
        AlanVoice.playText(this.response);
      });
    } else if (data["command"] == "message") {
      sendMessage(data["value"]).then((value) {
        this.response = value;
        setState(() {});
        AlanVoice.playText(this.response);
      });
    }
  }

  @override
  void dispose() {
    super.dispose();
    AlanVoice.removeButton();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xFFFFFFFF),
      body: SafeArea(
        child: Container(
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(10),
                      color: Colors.black,
                      image: DecorationImage(
                        fit: BoxFit.cover,
                        image: FileImage(File(widget.imagePath)),
                      ),
                    ),
                    margin: EdgeInsets.all(20),
                    width: 50,
                    height: 50,
                    // child: Transform(
                    //     alignment: Alignment.center,
                    //     child: FittedBox(
                    //       fit: BoxFit.cover,
                    //       child: Image.file(File(imagePath)),
                    //     ),
                    //     transform: Matrix4.rotationY(mirror)),
                  ),
                  Text(
                    'Hi ' + widget.username + '!',
                    style: TextStyle(fontSize: 22, fontWeight: FontWeight.w600),
                  ),
                ],
              ),
              Column(
                children: [
                  Container(
                    margin: EdgeInsets.all(20),
                    padding: EdgeInsets.all(20),
                    width: double.infinity,
                    decoration: BoxDecoration(
                      color: Color(0xFFFEFFC1),
                      borderRadius: BorderRadius.circular(10),
                    ),
                    child: Row(
                      children: [
                        Text(
                          '''You can:
1. Make calls.
2. Compose SMS.
3. Search web.
4. Ask about weather.
5. Use as calculator.
6. Use as calander.
                              ''',
                          style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                          textAlign: TextAlign.left,
                        ),
                        Spacer(),
                        ClipRRect(
                          borderRadius: BorderRadius.circular(10),
                          child: Image(
                            image: AssetImage("assets/logo.png"),
                            height: 90,
                          ),
                        )
                      ],
                    ),
                  ),
                  SizedBox(
                    height: 10,
                  ),
                  Card(
                    margin: EdgeInsets.symmetric(horizontal: 20),
                    elevation: 1,
                    child: Container(
                      width: double.infinity,
                      padding: EdgeInsets.all(16),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            "Response",
                            style: TextStyle(
                              fontSize: 12,
                              color: Colors.black45,
                            ),
                          ),
                          SizedBox(
                            height: 6,
                          ),
                          Text(
                            response ?? "Nothing yet..",
                            style: TextStyle(
                              color: response != null
                                  ? Colors.black
                                  : Colors.black45,
                              fontSize: 16,
                              fontStyle: response != null
                                  ? FontStyle.normal
                                  : FontStyle.italic,
                            ),
                          )
                        ],
                      ),
                    ),
                  )
                ],
              ),
              Spacer(),
              AppButton(
                text: "LOG OUT",
                onPressed: () {
                  AlanVoice.deactivate();
                  AlanVoice.removeButton();
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.pop(context);
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => MyHomePage()),
                  );
                },
                icon: Icon(
                  Icons.logout,
                  color: Colors.white,
                ),
                color: Color(0xFFFF6161),
              ),
              SizedBox(
                height: 20,
              )
            ],
          ),
        ),
      ),
    );
  }
}
