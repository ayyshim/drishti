import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:url_launcher/url_launcher.dart';

Future<String> _getPhoneNumber(String contactName) async {
  await Permission.contacts.request();

  Iterable<Contact> contacts =
      await ContactsService.getContacts(query: contactName);

  if (contacts.isEmpty) {
    throw Exception("Contact not found!");
  }

  final _contacts = contacts.toList();

  final _geniuneContact = _contacts.firstWhere((_co) => _co.phones.isNotEmpty);

  if (_geniuneContact == null) {
    throw Exception("Contact not found!");
  }

  return _geniuneContact.phones.first.value;
}

Future<String> makeCall(String contactName) async {
  try {
    final number = await _getPhoneNumber(contactName);

    launch("tel:$number");
    return "Calling $contactName";
  } catch (e) {
    return e.message;
  }
}

Future<String> webSearch(String query) async {
  launch("https://google.com/search?q=$query");
  return "Searching \"$query\"";
}

Future<String> sendMessage(String data) async {
  final dataToken = data.split("/");
  final contactName = dataToken[0];
  final message = dataToken[1];

  try {
    final number = await _getPhoneNumber(contactName);

    final uri = 'sms:$number?body=$message';

    launch(uri);
    return "Composing message to $contactName saying \"$message\"";
  } catch (e) {
    return e.message;
  }
}
