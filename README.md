# Drishti

## Install

- You must have flutter setup in your system.
- Run `flutter doctor`
- Run `install.bat` cmd batch file to download tensorflow model.

## Voice Commands

Call: Call <Person's name>

Message: Compose/Send SMS/message to <Person's name> <Message>

Search web: Search for <query>

Weather: What's weather like in <Place name>

Calculator: <1 + 1 | equation>

Calander: "What's today" will tell today's date.

> You must have Person's name in your contact list to make call and compose SMS. Calling and composing requires contact read permission.
